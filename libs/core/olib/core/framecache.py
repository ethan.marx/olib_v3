from dataclasses import dataclass

import numpy as np
from olib.gwftools import parse_frame_name


@dataclass
class FrameCache:
    """object for codifying framecache

    Args:
        ifo: name of the ifo of this cache
        frame_type: name of the frame type of this cache
    """

    ifo: str
    frame_type: str

    @property
    def frame_files(self):
        """List of paths to frame files"""
        return self._frame_files

    @property
    def start(self):
        """Earliest time of data in frame cache"""
        if self.n_frames == 0:
            return np.nan

        else:
            return self.frame_times[0]

    @property
    def stop(self):
        """Returns latest time of data in frame cache"""
        if self.n_frames == 0:
            return np.nan
        else:
            return self.frame_times[-1] + self.frame_lengths[-1]

    @property
    def frame_lengths(self):
        """Returns the lengths of the frames"""
        return self._frame_lengths

    @property
    def frame_times(self):
        """Returns the start times of the frames"""
        return self._frame_times

    @frame_files.setter
    def frame_files(self, files):
        """Setter function for frame files.
        Automatically parses the t0s and lengths of the frames
        """
        self._frame_files = files

        # parse files for lengths, and t0s
        self._frame_times = [parse_frame_name(frame)[1] for frame in files]
        # parse frame lengths for each frame, setting end times
        self._frame_lengths = [parse_frame_name(frame)[2] for frame in files]

    @property
    def n_frames(self):
        """Returns the number of frames"""
        return len(self.frame_files)

    def write(self, path):
        """Writes the cache to path in LIGO cache format"""

        with open(path, "w") as cache_file:
            # loop over frame files
            for i in range(self.n_frames):

                line = f"{self.ifo.strip('1')}"
                line += f" {self.frame_type}"
                line += f" {self.frame_times[i]}"
                line += f" {self.frame_lengths[i]}"
                line += f" {self.frame_files[i]}"
                line += "\n"
                cache_file.write(line)
