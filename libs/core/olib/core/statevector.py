from typing import List

import numpy as np
from gwpy.timeseries import StateTimeSeries, StateVector


def state_vector_to_segments(
    statevector: StateVector,
    bits: List[int],
):
    """Converts state vector to active segments corresponding to bitmask

    Args:
        statevector: gwpy StateVector object
        bits: List of which bits must be on for 'active' segment

    Returns:
        gwpy.segments.SegmentList: segments where bit mask is active

    """

    # convert statevector to dictionary of state time series
    state_bit_series_dict = statevector.get_bit_series(bits=bits)

    # perform logical "and" on state timeseries for all bits
    # returning a state time series

    # start with state_ts of all Trues
    state_ts = np.ones(len(statevector)).astype(bool)

    # for each bit time series
    for bit, array in state_bit_series_dict.items():
        # logical and
        state_ts &= array.value

    # create StateTimeSeries
    state_ts = StateTimeSeries(
        state_ts,
        t0=statevector.t0,
        dt=statevector.dt,
    )

    # convert to segment list
    segments = state_ts.to_dqflag().active

    # merge contiguous segments
    segments = segments.coalesce()

    return segments


def check_state_vector_for_bitmask(
    statevector: StateVector,
    bits: List[int],
):
    """Checks state vector given bitmask. Returns True
    if any sample is 'active'. Use case is checking for
    hardware injections in data.

    Args:
        statevector: gwpy StateVector object
        bits: List of which bits must be on for 'active' segment

    Returns:
        Boolean: whether bitmask is active for any sample

    """

    state_bit_series_dict = statevector.get_bit_series(bits=bits)
    state_ts = np.ones(len(statevector)).astype(bool)

    # loop over each bit time series,
    # performing logical and to ensure all bits denoting inj are on
    for bit, array in state_bit_series_dict.items():
        state_ts &= array.value

    # if at any sample in statevector is active
    flag = any(state_ts)

    return flag
