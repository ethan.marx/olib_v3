import os
from dataclasses import dataclass
from pathlib import Path
from typing import List

import numpy as np
from olib.gwftools import parse_frame_name


@dataclass
class Stride:
    """Class representing an oLIB analysis stride.

    Args:
        rundir: Path to main directory where run info is stored
        ifos: Which ifos this stride is analyzing
        start: gps start time of stride
        stop: gps end time of stride
    """

    rundir: Path
    ifos: List[str]
    start: float
    stop: float
    frame_types: dict[str, str]

    def __post_init__(self):
        # format name of directory
        subdir = (
            "".join(self.ifos)
            + "_"
            + str(int(self.start))
            + "_"
            + str(int(self.start - self.stop))
        )
        self.dir = self.rundir / f"{int(self.start // 100000)}" / subdir

        # make analysis stride directory
        # and subdirectories
        os.makedirs(self.dir, exist_ok=True)
        os.makedirs(self.dir / "framecache", exist_ok=True)
        os.makedirs(self.dir / "segments", exist_ok=True)

        # initiate framecache objects for each ifo
        self.framecaches = {}
        self.dqv_flags = {}
        self.inj_flags = {}
        self.success_flags = {}
        self.skip_flags = {}
        self.segments = {}
        for ifo in self.ifos:
            self.framecaches[ifo] = FrameCache(ifo, self.frame_types[ifo])
            self.success_flags[ifo] = False
            self.skip_flags[ifo] = False
            self.segments[ifo] = None
            self.inj_flags[ifo] = False
            self.dqv_flags[ifo] = False

    def filled_time(self, ifo: str):
        """True if data in FrameCache covers entire analysis stride"""

        if (
            self.framecaches[ifo].start <= self.start
            and self.framecaches[ifo].stop >= self.stop
        ):
            return True
        else:
            return False

    def livetime(self, ifo: str):
        """Calculates livetime of segments"""
        livetime = 0
        for segment in self.segments[ifo]:
            livetime += segment[1] - segment[0]
        return livetime

    @property
    def lost_start_time(self, ifo: str):
        """True if we no longer have the start time frame in FrameCache"""
        if self.framecaches[ifo].start > self.start:
            return True
        else:
            return False

    @property
    def ready_for_condor(self):
        ready = True
        for ifo in self.ifos:
            ready &= self.skip_flags[ifo] or self.success_flags[ifo]
        return ready


@dataclass
class FrameCache:
    """object for codifying framecache

    Args:
        ifo: name of the ifo of this cache
        frame_type: name of the frame type of this cache
    """

    ifo: str
    frame_type: str

    @property
    def frame_files(self):
        """List of paths to frame files"""
        return self._frame_files

    @property
    def start(self):
        """Earliest time of data in frame cache"""
        if self.n_frames == 0:
            return np.nan

        else:
            return self.frame_times[0]

    @property
    def stop(self):
        """Returns latest time of data in frame cache"""
        if self.n_frames == 0:
            return np.nan
        else:
            return self.frame_times[-1] + self.frame_lengths[-1]

    @property
    def frame_lengths(self):
        """Returns the lengths of the frames"""
        return self._frame_lengths

    @property
    def frame_times(self):
        """Returns the start times of the frames"""
        return self._frame_times

    @frame_files.setter
    def frame_files(self, files):
        """Setter function for frame files.
        Automatically parses the t0s and lengths of the frames
        """
        self._frame_files = files

        # parse files for lengths, and t0s
        self._frame_times = [parse_frame_name(frame)[1] for frame in files]
        # parse frame lengths for each frame, setting end times
        self._frame_lengths = [parse_frame_name(frame)[2] for frame in files]

    @property
    def n_frames(self):
        """Returns the number of frames"""
        return len(self.frame_files)

    def write(self, path):
        """Writes the cache to path in LIGO cache format"""

        with open(path, "w") as cache_file:
            # loop over frame files
            for i in range(self.n_frames):

                line = f"{self.ifo.strip('1')}"
                line += f" {self.frame_type}"
                line += f" {self.frame_times[i]}"
                line += f" {self.frame_lengths[i]}"
                line += f" {self.frame_files[i]}"
                line += "\n"
                cache_file.write(line)
