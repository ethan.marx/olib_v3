import numpy as np
from lalinference import BurstSineGaussian, BurstSineGaussianF


def olib_frequency_domain_sine_gaussian(
    frequency_array, hrss, q, frequency, phase, eccentricity
):
    """
    A wrapper to the oLIB source model in the frequency domain,
    convenient for use with bilby

    Args:
        frequency_array:
            frequencies at which to evaluate the model
        hrss:
            hrss of source
        q:
            quality factor of the burst, determines the width in frequency
        frequency:
            the central frequency of the burst
        phase:
            a reference phase parameter
        eccentricity:
            signal eccentricity, determines the relative fraction of each
            polarisation mode

    Returns:
        dict containing the two polarization modes
    """

    deltaF = frequency_array[1] - frequency_array[0]
    deltaT = 0.5 / (frequency_array[-1])

    # cast arguments as proper types for lalinference
    hrss = float(hrss)
    eccentricity = float(hrss)
    phase = float(phase)
    frequency = float(frequency)
    q = float(q)
    deltaT = float(deltaT)
    deltaF = float(deltaF)

    hplus, hcross = BurstSineGaussianF(
        q, frequency, hrss, eccentricity, phase, deltaF, deltaT
    )
    plus = np.zeros(len(frequency_array), dtype=complex)
    cross = np.zeros(len(frequency_array), dtype=complex)
    plus[: len(hplus.data.data)] = hplus.data.data
    cross[: len(hcross.data.data)] = hcross.data.data
    return dict(plus=plus, cross=cross)


def olib_time_domain_sine_gaussian(
    time_array, hrss, q, frequency, phase, eccentricity
):
    """
    A wrapper to the oLIB source model in the time domain,
    convenient for use with bilby

    Args:
        frequency_array:
            frequencies at which to evaluate the model
        hrss:
            hrss of source
        q:
            quality factor of the burst, determines the width in frequency
        frequency:
            the central frequency of the burst
        phase:
            a reference phase parameter
        eccentricity:
            signal eccentricity, determines the relative fraction of each
            polarisation mode

    Returns:
        dict containing the two polarization modes
    """

    deltaT = time_array[1] - time_array[0]

    # cast arguments as proper types for lalinference
    hrss = float(hrss)
    eccentricity = float(hrss)
    phase = float(phase)
    frequency = float(frequency)
    q = float(q)
    deltaT = float(deltaT)

    hplus, hcross = BurstSineGaussian(
        q, frequency, hrss, eccentricity, phase, deltaT
    )

    plus = np.zeros(len(time_array))
    cross = np.zeros(len(time_array))
    plus[: len(hplus.data.data)] = hplus.data.data
    cross[: len(hcross.data.data)] = hcross.data.data

    return dict(plus=plus, cross=cross)
