import os
from pathlib import Path

from glue.pipeline import CondorDAGJob


class BaseJob(CondorDAGJob):
    def __init__(
        self,
        log_dir: Path,
        executable: Path,
        cp,
        section,
        accounting_group_user,
        accounting_group=None,
    ):
        CondorDAGJob.__init__(self, "vanilla", executable)

        # These are all python jobs so need to pull in the env
        self.add_condor_cmd("getenv", "True")
        log_base = os.path.join(
            log_dir, os.path.basename(executable) + "-$(cluster)-$(process)"
        )
        self.set_stderr_file(log_base + ".err")
        self.set_stdout_file(log_base + ".out")
        self.set_sub_file(section + ".sub")

        if cp is not None:
            self.add_ini_opts(cp, section)

        if accounting_group:
            self.add_condor_cmd("accounting_group", accounting_group)
