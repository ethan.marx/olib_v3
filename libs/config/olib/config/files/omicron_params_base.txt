//*************************************************************************************
//************************   Omicron configuration file   *****************************
//*************************************************************************************
 
 
//*************************************************************************************
//************************           DATA CLASS           *****************************
//*************************************************************************************
 
//** path to a frame file list file (FFL or lalcache format)
DATA    LCF    FRAMECACHE 2 2
 
//** list of channels you want to process (here you have 3).
//   They can be listed on one single line or several lines
DATA    CHANNELS    CHANNELNAME

//** working sampling frequency (one value for all channels)
DATA    SAMPLEFREQUENCY SAMPFREQ
 
//************************************************************************************
//************************         PARAMETER CLASS       *****************************
//************************************************************************************
 
//** analysis window duration and overlap in seconds
PARAMETER   TIMING      WINDOW  OVERLAP
 
//** search frequency range [Hz]
PARAMETER       FREQUENCYRANGE  MINFREQ      MAXFREQ
 
//** search Q range
PARAMETER       QRANGE          MINQ  MAXQ
 
//** maximal mismatch between tiles
PARAMETER   MISMATCHMAX 0.2
 
//** tile SNR threshold
PARAMETER   SNRTHRESHOLD   THRESHSNR
 
//*************************************************************************************
//************************           OUTPUT CLASS         *****************************
//*************************************************************************************
 
//** path to output directory
OUTPUT     DIRECTORY       OUTPUTDIR
 
//** list of data products
OUTPUT     PRODUCTS    triggers
 
//** output file format
OUTPUT     FORMAT      hdf5
 
//** verbosity level (0-1-2-3)
OUTPUT     VERBOSITY       0
