from pathlib import Path
from typing import Optional


def write_omicron_param_file(
    framecache: Path,
    channel: str,
    sample_freq: float,
    window: float,
    overlap: float,
    fmin: float,
    fmax: float,
    qmin: float,
    qmax: float,
    snr_thresh: float,
    trigger_dir: Path,
    output_path: Path,
    injection_channel: Optional[str] = None,
    injection: Optional[bool] = False,
):
    """Writes parameter file for omicron run"""

    # dictionary to store text to replace
    key_dict = {
        "FRAMECACHE": framecache,
        "CHANNELNAME": channel,
        "SAMPFREQ": sample_freq,
        "WINDOW": window,
        "OVERLAP": overlap,
        "MINFREQ": fmin,
        "MAXFREQ": fmax,
        "MINQ": qmin,
        "MAXQ": qmax,
        "THRESHSNR": snr_thresh,
        "OUTPUTDIR": trigger_dir,
    }

    # base omicron param file
    file = "omicron_params_base.txt"

    # if injection, add injection channel
    # and change base file
    if injection:
        key_dict["INJCHANNEL"] = injection_channel
        file = "omicron_params_inj.txt"

    # read in data and loop over keys, replacing them in file
    with open(Path("files") / file, "r") as f:
        data = f.read()
        for key, value in key_dict.items():
            data = data.replace(key, str(value))

    # write file to output path
    with open(output_path, "w") as f:
        f.write(data)
