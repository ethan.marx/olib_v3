import os
import shutil
from pathlib import Path

import pytest
from gwpy.timeseries import TimeSeries
from olib.executables.inject_training import main

TEST_DIR = Path(__file__).resolve().parent


@pytest.fixture(params=[["H1", "L1"]])
def ifos(request):
    return request.param


@pytest.fixture()
def cache_files(ifos, data_dir):
    cache_files = {}
    start = 1185587200
    stop = 1185591296 + 1e-5
    length = int(stop - start)

    for ifo in ifos:
        frame_path = (
            data_dir
            / f"{ifo.strip('1')}-{ifo}_test_frame-{start}-{length}.gwf"
        )
        frame = TimeSeries.fetch_open_data("H1", start, stop)

        frame.write(frame_path)

        cache_file = str(data_dir / f"{ifo}.lcf")
        cache_files[ifo] = cache_file
        with open(cache_file, "w") as f:
            f.write(
                f"{ifo.strip('1')} test_frame 1185587200 4096 {frame_path}"
            )

    return cache_files


@pytest.fixture(scope="session")
def data_dir():
    data_dir = "tmp"
    os.makedirs(data_dir, exist_ok=True)
    yield Path(data_dir)
    shutil.rmtree(data_dir)


@pytest.fixture(params=["prior.prior"])
def prior_file(request):
    return str(TEST_DIR / "priors" / request.param)


def test_main(
    ifos,
    prior_file,
    cache_files,
    data_dir,
):

    start = 1185587200
    stop = 1185591296
    sample_freq = 2048
    overlap = 2
    inj_spacing = 100
    channels = {"H1": "Strain", "L1": "Strain"}

    main(
        ifos,
        start,
        stop,
        sample_freq,
        overlap,
        inj_spacing,
        prior_file,
        cache_files,
        channels,
        data_dir,
    )
