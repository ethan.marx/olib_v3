import os
from pathlib import Path
from typing import List

import bilby
import h5py
import numpy as np
from gwpy.timeseries import TimeSeries
from hermes.typeo import typeo
from olib.core.sg import olib_time_domain_sine_gaussian


@typeo
def main(
    ifos: List[str],
    start: float,
    stop: float,
    sample_freq: float,
    overlap: float,
    inj_spacing: float,
    prior_file: Path,
    cache_files: dict[str, str],
    channels: dict[str, str],
    outdir: Path,
):
    """
    Injects Sine Gaussian Waveforms into cache of real data

    Args:
        ifos:
        start:
        stop:
        sample_freq:
        overlap:
        inj_spacing:
        prior_file:
        cache_files:
        channels:
        outdir:

    """

    # make output folder
    os.makedirs(outdir, exist_ok=True)

    # create bilby waveform generator object
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=2,  # hard coded
        sampling_frequency=sample_freq,
        time_domain_source_model=olib_time_domain_sine_gaussian,
    )

    # duration of segment
    length = stop - start

    # based on injection spacing, create list of times
    # to inject data at
    injection_times = np.arange(
        start + overlap, stop - overlap, step=inj_spacing
    ).astype("float64")

    # add some jitter to times
    injection_times += np.random.normal(
        loc=0, scale=1, size=len(injection_times)
    )

    # load in injection priors and sample
    priors = bilby.gw.prior.PriorDict(prior_file)

    injection_parameters = priors.sample(len(injection_times))

    # update injection times
    injection_parameters["geocent_time"] = injection_times

    # save parameters of injections to h5 file
    with h5py.File(outdir / "injection_parameters.h5", "w") as f:
        for key, values in injection_parameters.items():
            f.create_dataset(key, data=values)

    # reshape to list of dicts for each event
    injection_parameters = [
        dict(zip(injection_parameters, col))
        for col in zip(*injection_parameters.values())
    ]

    for ifo in ifos:

        # load in data from cache via gwpy
        data = TimeSeries.read(
            cache_files[ifo], start=start, end=stop, channel=channels[ifo]
        )
        data = data.resample(sample_freq)

        # initialize bilby interferometer object
        bilby_ifo = bilby.gw.detector.networks.get_empty_interferometer(ifo)

        # loop over times injecting random waveforms into data
        for i, p in enumerate(injection_parameters):

            # sample waveform from waveform generator
            polarizations = waveform_generator.time_domain_strain(p)

            ra = p["ra"]
            dec = p["dec"]
            geocent_time = p["geocent_time"]
            psi = p["psi"]

            injection = np.zeros(len(polarizations["cross"]))
            for mode, polarization in polarizations.items():

                response = bilby_ifo.antenna_response(
                    ra, dec, geocent_time, psi, mode
                )
                injection += response * polarization[i]

            # TODO: Is the geocent time being set correctly ?
            injection = TimeSeries(
                injection, dt=1 / sample_freq, t0=geocent_time
            )

            # set injection t0 to closest value in data
            closest_time_arg = np.argmin(np.abs(data.times - injection.t0))
            injection.t0 = data.times[closest_time_arg]

            # inject signal into gwpy timeseries
            data = data.inject(injection)

        # save as one gwf frame
        outpath = outdir / f"{ifo}-DatInjMerge-{start}-{length}.gwf"
        data.write(outpath)

        # write file to cache
        with open(cache_files[ifo], "w") as cache:
            line = f"{ifo} "
            line += "DatInjMerge "
            line += "{start} "
            line += "{length} "
            line += "file://localhost/{outpath}\n"
            cache.write(line)
