import logging
import os
import time
from pathlib import Path
from typing import List, Optional

import numpy as np
from gwdatafind import find_urls
from gwpy.timeseries import StateVector
from hermes.typeo import typeo
from lal import gpstime
from olib.core import Stride, statevector
from olib.logging import configure_logging


@typeo
def main(
    rundir: Path,
    ifos: List[str],
    stride: int,
    lib_stride: int,
    overlap: int,
    wait: int,
    max_wait: int,
    inj_runmode: bool,
    dqv_runmode: bool,
    train_runmode: bool,
    frame_types: dict[str, str],
    channels: dict[str, str],
    state_channels: dict[str, str],
    dqv_channels: dict[str, str],
    run_bits: List[int],
    hwinj_bits: List[int],
    dqv_bits: List[int],
    initial_start: Optional[float] = None,
    verbose: bool = False,
):
    """Launches the online oLIB pipeline

    Args:
        rundir: Main directory for storing analysis results
        ifos: List of ifos to analyze
        stride: length of time to analyze at once
        lib_stride: Not sure...
        overlap: overlap between consecutive strides
        wait: how long to wait before re-quering data
        max_wait:
            maximum amount of time to wait before moving on to next stride
        inj_runmode: True if running only on times flagged with HW injections
        dq_runmode: True if running only on times flagged w/ DQ vetoes
        train_runmode: True if injecting SG waveforms to train LLRT
        frame_types: frame types for querying data
        channels: hoft channels
        state_channels: channels to check for science mode
        dqv_channels: channels to check for data_quality vetoes
        run_bits: List of bits that denote science mode
        hwinj_bits: List of bits that denote presence of HW injection
        dqv_bits: List of bits that denote a data quality veto
        initial_start: start time of analysis
        verbose: denotes logging at the debug level
    """

    os.makedirs(rundir, exist_ok=True)
    configure_logging(verbose=verbose)

    # if no initial start passed, use the current gpstime
    if initial_start is None:
        initial_start = gpstime.gps_time_now().gpsSeconds

    logging.info(f"Starting oLIB pipeline at time {initial_start}")

    # store current start
    np.savetxt(rundir / "current_start.txt", np.array([initial_start]))

    start = initial_start
    stop = start + stride
    running_wait = 0

    # initialize first AnalysisStride object
    analysis_stride = Stride(rundir, ifos, start, stop, frame_types)

    # start a while loop
    while True:
        # wait for designated amount of time
        # TODO: is this to make sure frames appear in stream?
        # better way to handle streaming data?

        # sleep for designated wait_time
        time.sleep(wait)

        # add to running wait
        running_wait += wait

        for ifo in ifos:

            logging.info(
                f"Querying data between {analysis_stride.start} "
                " and {analysis_stride.stop}"
            )

            # query data for ifo and set frame files
            # attribute in FrameCache object
            analysis_stride.framecaches[ifo].frame_files = find_urls(
                site=ifo.strip("1"),
                frametype=frame_types[ifo],
                gpsstart=analysis_stride.start - lib_stride,
                gpsend=analysis_stride.stop + lib_stride,
                urltype="file",
            )

            # write the cache
            analysis_stride.framecaches[ifo].write(
                analysis_stride.dir / "framecache" / f"{ifo}.cache"
            )

            # check if we filled the stride with frames
            if analysis_stride.filled_time(ifo):

                # read in the statevector
                state_vector = StateVector.read(
                    analysis_stride.framecaches[ifo].frame_files,
                    channel=f"{ifo}:{state_channels[ifo]}",
                    start=analysis_stride.start,
                    end=analysis_stride.stop,
                    bits=run_bits,
                )

                # read in the statevector w/ inj bits
                hwinj_vector = StateVector.read(
                    analysis_stride.framecaches[ifo].frame_files,
                    channel=f"{ifo}:{state_channels[ifo]}",
                    start=analysis_stride.start,
                    end=analysis_stride.stop,
                    bits=hwinj_bits,
                )

                # read in data quality vector
                dq_vector = StateVector.read(
                    analysis_stride.framecaches[ifo].frame_files,
                    channel=f"{ifo}:{dqv_channels[ifo]}",
                    start=analysis_stride.start,
                    end=analysis_stride.stop,
                    bits=dqv_bits,
                )

                # check data quality
                analysis_stride.dqv_flags[
                    ifo
                ] = statevector.check_state_vector_for_bitmask(
                    dq_vector, bits=dqv_bits
                )

                # check for hw inj status
                analysis_stride.inj_flags[
                    ifo
                ] = statevector.check_state_vector_for_bitmask(
                    hwinj_vector, bits=hwinj_bits
                )

                # convert state vector to segments
                analysis_stride.segments[
                    ifo
                ] = statevector.state_vector_to_segments(
                    state_vector, bits=run_bits
                )

                # check if data quality flag corresponds
                # to data quality run mode
                skip_flag = (
                    analysis_stride.dqv_flags[ifo] and not dqv_runmode
                ) or (not analysis_stride.dqv_flags[ifo] and dqv_runmode)

                # check if hw injection flag corresponds to inj run mode
                skip_flag |= (
                    analysis_stride.inj_flags[ifo] and not inj_runmode
                ) or (not analysis_stride.inj_flags[ifo] and inj_runmode)

                # if there are no segments to analyze for this ifo, skip
                skip_flag |= analysis_stride.livetime(ifo) == 0

                # set skip flag for ifo
                analysis_stride.skip_flags[ifo] = skip_flag

                # set success flag for ifo
                if not skip_flag:
                    analysis_stride.success_flags[ifo] = True

                if analysis_stride.ready_for_condor:
                    # write and submit condor dag
                    logging.info(
                        "Writing and submitting dag for"
                        " analysis stride to condor"
                    )
                    # olib.pipeline.write_and_submit_dag(analysis_stride.dir)

                    # update stride
                    start += stride
                    stop = start + stride

                    np.savetxt(rundir / "current_start.txt", np.array([start]))
                    running_wait = 0

                    # initialize first AnalysisStride object
                    analysis_stride = Stride(
                        rundir, ifos, start, stop, frame_types
                    )

            # check if we have lost the start time frame
            # set skip flag to True
            elif analysis_stride.lost_start_time:
                logging.info(
                    f"Lost start time frame, skipping segment for {ifo}"
                )
                analysis_stride.skip_flags[ifo] = True

                if analysis_stride.ready_for_condor:
                    # write and submit condor dag
                    logging.info(
                        "Writing and submitting dag for"
                        " analysis stride to condor"
                    )
                    # dag = olib.pipeline.write_and_submit_dag()

                    # update stride
                    start += stride
                    stop = start + stride

                    np.savetxt(rundir / "current_start.txt", np.array([start]))
                    running_wait = 0

                    # initialize first AnalysisStride object
                    analysis_stride = Stride(
                        rundir, ifos, start, stop, frame_types
                    )


if __name__ == "__main__":
    main()
